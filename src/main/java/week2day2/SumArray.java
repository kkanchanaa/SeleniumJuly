package week2day2;

import java.util.Scanner;

public class SumArray {
	public static void main(String[] args) {
		Scanner sobj=new Scanner(System.in);
		System.out.println("Enter the number of elements in array: ");
		int n=sobj.nextInt();
		System.out.println("Enter the values: ");
		int arr[]=new int[n];
		int a=0;
		for(int i=0;i<n;i++)
		{
			arr[i]=sobj.nextInt();
			a=a+arr[i];
		}
		System.out.println("Sum of elements in array: "+a);
	sobj.close();
	}
}
