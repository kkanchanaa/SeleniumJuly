package week2day2;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
public class MapTv {
	public static void main(String[] args) {
		Map<String,Integer> tvobj= new LinkedHashMap<String,Integer>();
		tvobj.put("LG",2);
		tvobj.put("Samsung",1);
		tvobj.put("onida", 1);	
		//number of tvs
		int a=0;
		for(Entry<String,Integer> eachtv : tvobj.entrySet())
		{
		a=a+eachtv.getValue();
		}
		System.out.println("Number of TV items: "+a);
		//lasttv name
		Set<String> alltvs=tvobj.keySet();
		List<String> alltele=new ArrayList<String>();
		alltele.addAll(alltvs);
		System.out.println("Last TV name: "+alltele.get(alltele.size()-1));
		//Last TV model count
		Integer countolasttv=tvobj.get(alltele.get(alltele.size()-1));
		System.out.println("Last TV count: "+countolasttv); 
		//Delete one count from 1ast model
		tvobj.put(alltele.get(alltele.size()-1),countolasttv-1);
		System.out.println(tvobj);
		}
	}

