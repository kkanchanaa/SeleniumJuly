package week2day2;

import java.util.Scanner;

public class Exceptions {
	public static void main(String[] args) {
		int arr[]= {1,2,3,4,5};
		/*try
		{
		System.out.println(arr[5]);
		}		
		catch(ArrayIndexOutOfBoundsException e)
		{
		System.out.println("Invalid input");
		}*/
		Scanner scexe=new Scanner(System.in);
		System.out.println("Enter the index value: ");
		int arrindex=scexe.nextInt();
		try 
		{
				System.out.println(arr[arrindex]);
		}
		catch(Exception ArrayIndexOutOfBoundException)
		{
			System.out.println("Invalid input");
		}
		scexe.close();
	}
}
