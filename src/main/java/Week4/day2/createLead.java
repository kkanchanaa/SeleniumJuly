package Week4.day2;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.WebElement;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
public class createLead extends ProjectMethods{
	public void starttestcase() {
		testcaseName="createlead";
		tescaseDescription="createlead";
		author="test1";
		category="smoke";
		excelfilename="createlead";
	}
	@Test(/*groups="Smoke",*/dataProvider="lead")
	public void login(String name,String fname,String lname,String mobnum,String mail)
	{
		WebElement elecrm = locateElement("link", "CRM/SFA");
		click(elecrm);
		WebElement elecrlead = locateElement("link", "Create Lead");
		click(elecrlead);
		WebElement elecomname = locateElement("id", "createLeadForm_companyName");
		type(elecomname, name);
		WebElement elefirst = locateElement("id","createLeadForm_firstName");
		type(elefirst, fname);
		WebElement elelast = locateElement("id","createLeadForm_lastName");
		type(elelast, lname);
		WebElement elephone = locateElement("id","createLeadForm_primaryPhoneNumber");
		type(elephone, mobnum);
		driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);
		WebElement eleemail = locateElement("createLeadForm_primaryEmail");
		type(eleemail,mail);
//		WebElement elesource = locateElement("id","createLeadForm_dataSourceId");
//		selectDropDownUsingText(elesource, "Conference");
//		WebElement elemkt = locateElement("id","createLeadForm_marketingCampaignId");
//		selectDropDownUsingText(elemkt, "Automobile");
//		WebElement eleind = locateElement("id", "createLeadForm_industryEnumId");
//		selectDropDownUsingIndex(eleind, 10);
WebElement elecrsub = locateElement("name", "submitButton");
		click(elecrsub);
//		WebElement elefnlead = locateElement("id", "viewLead_firstName_sp");
//		verifyExactText(elefnlead, "Test");	
	}
	
}
