package Week4.day2;

import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;


public class ExtentrepAnnotation {
	public static ExtentTest Test;
	public static ExtentReports extent;
	public static String testcaseName,tescaseDescription,author,category,excelfilename;
		@BeforeSuite
		public void startresult() {
		ExtentHtmlReporter html=new ExtentHtmlReporter("./reports/result.html");
		html.setAppendExisting(true);
		extent= new ExtentReports();
		extent.attachReporter(html);
		}
		@BeforeMethod
		public void starttestcase()
		{
		Test = extent.createTest(testcaseName,tescaseDescription);
		Test.assignAuthor(author);
		Test.assignCategory(category);
		}
		public void reportStep(String desc,String status)
		{
			if(status.equalsIgnoreCase("pass"))
			{
				Test.pass(desc);}
			if(status.equalsIgnoreCase("fail")) {
				Test.fail(desc);
			}
		}
		@AfterSuite
		public void stopresult() {
		extent.flush();
		}
}
