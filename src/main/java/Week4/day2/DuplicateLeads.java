package Week4.day2;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;
public class DuplicateLeads extends SeMethods{
	@Test
	public void duplicate()
	{
		startApp("chrome", "http://leaftaps.com/opentaps");
		WebElement eleUserName = locateElement("id", "username");
		type(eleUserName,"DemoSalesManager");
		WebElement elePassword = locateElement("id","password");
		type(elePassword, "crmsfa");
		WebElement eleLogin = locateElement("class","decorativeSubmit");
		click(eleLogin);
		WebElement ele4 = locateElement("link", "CRM/SFA");
		click(ele4);
		WebElement ele5 = locateElement("link", "Leads");
		click(ele5);
		WebElement ele6 = locateElement("link", "Find Leads");
		click(ele6);
		WebElement ele7 = locateElement("xpath","//ul[contains(@class,'top')]/li[3]");
		click(ele7);
		WebElement ele8 = locateElement("name","emailAddress");
		type(ele8,"a@test.com");
		WebElement ele9 = locateElement("xpath","//button[contains(text(),'Find Leads')]");
		click(ele9);
		WebElement ele10 = locateElement("xpath","//table[contains(@class,'row')]//a");
		String capture=getText(ele10);
		System.out.println("Captured Id is:"+capture);
		click(ele10);
		WebElement ele11 = locateElement("link","Duplicate Lead");
		click(ele11);
		verifyTitle("Duplicate Lead");
		WebElement ele12 = locateElement("link","Create Lead");
		click(ele12);
	}
}
