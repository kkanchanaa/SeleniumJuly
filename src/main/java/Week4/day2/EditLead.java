package Week4.day2;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;
public class EditLead extends SeMethods {
	@Test//(groups="sanity",dependsOnGroups="smoke",enabled=false)
	public void edit() throws InterruptedException
	{
		startApp("chrome", "http://leaftaps.com/opentaps");
		WebElement eleUserName = locateElement("id", "username");
		type(eleUserName, "DemoSalesManager");
		WebElement elePassword = locateElement("id","password");
		type(elePassword, "crmsfa");
		WebElement eleLogin = locateElement("class","decorativeSubmit");
		click(eleLogin);
		WebElement ele4 = locateElement("link", "CRM/SFA");
		click(ele4);
		WebElement ele5 = locateElement("link", "Leads");
		click(ele5);
		WebElement ele6 = locateElement("link", "Find Leads");
		click(ele6);
		WebElement ele7 = locateElement("xpath", "(//input[@name='firstName'])[3]");
		type(ele7,"Test");
		WebElement ele8 = locateElement("xpath", "//button[text()='Find Leads']");
		click(ele8);
		WebElement ele9 = locateElement("xpath", "//table[contains(@class,'row')]//a");
		click(ele9);
		verifyTitle("View Lead | opentaps CRM");
		WebElement ele10 = locateElement("link","Edit");
		click(ele10);
		WebElement ele11 = locateElement("id","updateLeadForm_companyName");
		ele11.clear();
		type(ele11,"Test leaf update");
		WebElement ele12 = locateElement("xpath","//input[@name='submitButton'][1]");
		click(ele12);
	}

}
