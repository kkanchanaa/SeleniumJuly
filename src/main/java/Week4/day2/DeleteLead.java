package Week4.day2;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

public class DeleteLead extends SeMethods {
	@Test//(groups="Regression",dependsOnGroups="sanity")
	public void delete()
	{
		startApp("chrome", "http://leaftaps.com/opentaps");
		WebElement eleUserName = locateElement("id", "username");
		type(eleUserName,"DemoSalesManager");
		WebElement elePassword = locateElement("id","password");
		type(elePassword, "crmsfa");
		WebElement eleLogin = locateElement("class","decorativeSubmit");
		click(eleLogin);
		WebElement ele4 = locateElement("link", "CRM/SFA");
		click(ele4);
		WebElement ele5 = locateElement("link", "Leads");
		click(ele5);
		WebElement ele6 = locateElement("link", "Find Leads");
		click(ele6);
		WebElement ele9 = locateElement("xpath", "//ul[contains(@class,'top')]/li[2]");
		click(ele9);
		WebElement ele10 = locateElement("name","phoneNumber");
		type(ele10, "123456789");
		WebElement ele11 = locateElement("xpath", "//button[contains(text(),'Find Leads')]");
		click(ele11);
		WebElement ele12 = locateElement("xpath", "//table[contains(@class,'row')]//a");
		String capture=getText(ele12);
		System.out.println("Captured Id is:"+capture);
		click(ele12);
		WebElement ele13= locateElement("link", "Delete");
		click(ele13);	
		WebElement ele14 = locateElement("link", "Find Leads");
		click(ele14);
		WebElement ele15 = locateElement("xpath","//input[@name='id']");
		type(ele15,capture);	
		WebElement ele16 = locateElement("xpath","//button[contains(text(),'Find Lead')]");
		click(ele16);
		WebElement ele17 = locateElement("xpath","//div[contains(text(),'No records to display')]");
		verifyExactText(ele17,"No records to display");
	}
}