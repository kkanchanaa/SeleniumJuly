package Week4.day2;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;

import Week5.Day2.exceldata;
public class ProjectMethods extends SeMethods{
	@DataProvider(name="lead")
	public Object[][] getdata()
	{
		Object[][] data =exceldata.getexceldata(excelfilename);
		return data;
	}
	@BeforeSuite//(groups="common")
	@Parameters({"browser","appurl","username","password"})
	public void loginsub(String browser,String url, String username,String password)
	{
		startApp(browser, url);
		WebElement eleUserName = locateElement("id", "username");
		type(eleUserName, username);
		WebElement elePassword = locateElement("id","password");
		type(elePassword, password);
		WebElement eleLogin = locateElement("class","decorativeSubmit");
		click(eleLogin);
	}
//	@AfterSuite
//	public void close()
//	{
//		closeBrowser();
//	}
}
