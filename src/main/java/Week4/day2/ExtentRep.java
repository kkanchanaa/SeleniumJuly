package Week4.day2;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class ExtentRep {
	public static void main(String[] args) {
		ExtentHtmlReporter html=new ExtentHtmlReporter("./reports/result.html");
		html.setAppendExisting(true);
		ExtentReports extent= new ExtentReports();
		extent.attachReporter(html);
		ExtentTest Test = extent.createTest("Tc_001","Create Lead");
		Test.pass("success",MediaEntityBuilder.createScreenCaptureFromPath("").build());
		Test.pass("Success");
		Test.pass("entered successfully:");
		Test.assignAuthor("kanch");
		Test.assignCategory("Regresion");
		extent.flush();
		
	}


}

