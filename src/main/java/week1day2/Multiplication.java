package week1day2;

import java.util.Scanner;

public class Multiplication {
public static void main(String[] args) {
	System.out.println("Enter Number#1:");
	Scanner sc1=new Scanner(System.in);
	int n1=sc1.nextInt();
	System.out.println("Enter Number#2:");
	int n2=sc1.nextInt();
	if(n1>0 && n2>0)
	{
	System.out.println("The multiplication table for " + n1 +" is");
		for(int i=1;i<=n2;i=i+1)
		{
			int n3=i*n1;
		System.out.println(i+"*"+n1+"="+n3);
		}
	}
	else
		System.out.println("Invalid input: Give input above 0");
	sc1.close();
}
}
