package week1day2;

import java.util.Scanner;

public class Fibonacci {
public static void main(String[] args) {
	System.out.println("Enter the number:");
	Scanner scfib=new Scanner(System.in);
	int n=scfib.nextInt();
	int a=0,b=1,c=1;
	System.out.println("Fibonacci series:");
	for(int i=1;i<=n;i++)
	{
		a=b;
		b=c;
		c=a+b;
		System.out.println(a);
	}
	scfib.close();
}
}
