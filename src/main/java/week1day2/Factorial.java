package week1day2;

import java.util.Scanner;

public class Factorial {
public static void main(String[] args) {
	Scanner scfact=new Scanner(System.in);
	System.out.println("Enter the number:");
	int n= scfact.nextInt();
	int i, fact=1;
	for(i=1;i<=n;i++)
	{
		fact=fact*i;
	}
	System.out.println("Factorial of "+n+ " is "+fact);
	scfact.close();
}
}
