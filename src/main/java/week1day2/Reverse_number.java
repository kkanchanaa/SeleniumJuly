package week1day2;

import java.util.Scanner;

public class Reverse_number {
public static void main(String[] args) {
	Scanner screv=new Scanner(System.in);
	System.out.println("Enter the number:");
	int n=screv.nextInt();
	int temp,sum=0,r;
	temp=n;
	while(n>0)
	{
		r=n%10;
		sum=sum*10+r;
		n=n/10;
	}
	System.out.println("The reverse of number is:"+sum);
	screv.close();
}
}
