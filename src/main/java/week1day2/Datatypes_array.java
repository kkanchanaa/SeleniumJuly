package week1day2;

public class Datatypes_array {
	public static void main(String[] args)
	{
		boolean[] d1={true,false};
		for(boolean eachd1:d1) 
		{
			System.out.println("Boolean: "+eachd1);
		}
		byte[] d2= {127,-126};
		for(byte eachd2:d2)
		{
			System.out.println("Byte: "+eachd2);
		}
		short[] d3= {-32768,32767};
		for(short eachd3:d3)
		{
			System.out.println("Short: "+eachd3);
		}
		int[] d4= {15241,2154};
		for(int eachd4:d4)
		{
			System.out.println("int: "+eachd4);
		}
		long[] d5= {1234567854,1234567890};
		for(long eachd5:d5)
		{
			System.out.println("long: "+eachd5);
		}
		float[] d6= {12.12f,2.15f};
		for(float eachd6:d6)
		{
			System.out.println("float: "+eachd6);
		}
		double[] d7= {124.15,23.215515};
		for(double eachd7:d7)
		{
			System.out.println("Double: "+eachd7);
		}
		char[] d8= {'a','h'};
		for(char eachd8:d8)
		{
		System.out.println("char: "+eachd8);	
		}
		}
}
