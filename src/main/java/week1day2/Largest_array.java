package week1day2;
import java.util.Scanner;
public class Largest_array 
{
	public static void main(String[] args) 
	{
		int i, n,temp;
		Scanner sclar=new Scanner(System.in);
		System.out.println("Enter the number of elements in array:");
		n=sclar.nextInt();
		System.out.println("Enter the numbers:");
		int larg[]=new int[n];
		//for getting values in each element of array
		for(i=0;i<n;i++)
		{
			larg[i]=sclar.nextInt();
			
		}
		//for descending order
		for(i=0;i<n;i++)
		{
			for(int j=i+1;j<n;j++)
			{
				if(larg[j]>larg[i])
			{
			temp=larg[i];
			larg[i]=larg[j];
			larg[j]=temp;
			}
		}
		}
		System.out.println("First largest number is "+ larg[0]);
		System.out.println("Third largest number is "+ larg[2]);
		sclar.close();
		}
}
