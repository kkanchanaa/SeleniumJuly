package week1day2;
import java.util.Scanner;
public class Month_year {
	public static void main(String[]  args)
	{
		System.out.println("Enter a number:");
		Scanner scm=new Scanner(System.in);
		int m1=scm.nextInt();
		String[] month= {"January","February","March","April","May","June","July","August",
				"September","October","November","December"};
		int days[]= {31,28,31,30,31,30,31,30,31,30,31,30};
		if(m1>0 && m1<13)
		{
			int m2=m1-1;
		System.out.println("Month of the year is "+ month[m2]);
		System.out.println("Number of days in the month of "+month[m2]+ " is "+days[m2]);
		}
		else
			System.out.println("Invalid input- Give input between 1 to 12");
		scm.close();
	}	
}
