package week2day1;
import java.util.LinkedHashSet;
import java.util.Set;

public class CharSet {
	public static void main(String[] args) {
		String	struniq="cognizant india";
		System.out.println("Given String: "+struniq);
		Set<Character> charobj=new LinkedHashSet<Character>();
		for(char charuniq:struniq.toCharArray())
		{
			charobj.add(charuniq);
		}
		System.out.println("Unique characters of the string is : \n"+charobj);
	}
}