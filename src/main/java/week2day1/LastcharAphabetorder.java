package week2day1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class LastcharAphabetorder {
public static void main(String[] args) {
	String nstring="i learned a lot today";
	System.out.println("Given String:"+nstring);
	List<Character> lastobj=new ArrayList<Character>();
	for(char eachstr:nstring.toCharArray())
	{
	lastobj.add(eachstr);	
	}
	Collections.sort(lastobj);
	System.out.println("Alphabetical order:"+lastobj);
	System.out.println("Last character of the given string(arranged in alphabetical order) is: "
	+lastobj.get(lastobj.size()-1));
}
}
