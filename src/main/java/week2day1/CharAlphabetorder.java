package week2day1;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class CharAlphabetorder {
	public static void main(String[] args) 
	{
		String	struniq="amazon development center";
		System.out.println("Given string: "+struniq);
		List<Character> charobj1= new ArrayList<Character>();
		for(char charuniq:struniq.toCharArray())
		{
			charobj1.add(charuniq);
		}
		Collections.sort(charobj1);
		System.out.println("Given string in alphabetical order(includes duplicate values):\n"+charobj1);
	}
}
