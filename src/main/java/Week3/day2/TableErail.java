package Week3.day2;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class TableErail {
public static void main(String[] args) {
	System.setProperty("webdriver.chrome.driver","./Driver/chromedriver.exe");
	ChromeDriver driver=new ChromeDriver();
	driver.get("http://erail.in");
	driver.findElementById("txtStationFrom").clear();
	driver.findElementById("txtStationFrom").sendKeys("MAS",Keys.TAB);
	driver.findElementById("txtStationTo").clear();
	driver.findElementById("txtStationTo").sendKeys("MDU",Keys.TAB);
	boolean selected = driver.findElementById("chkSelectDateOnly").isSelected();
	if(selected)
	{
		driver.findElementById("chkSelectDateOnly").click();
	}
	WebElement tablename = driver.findElementByXPath("//table[@class='DataTable DataTableHeader TrainList']");
	List<WebElement> row = tablename.findElements(By.tagName("tr"));
	for (WebElement eachrow : row) {
		List<WebElement> col = eachrow.findElements(By.tagName("td"));
		System.out.println(col.get(1).getText());
	}
}
}
