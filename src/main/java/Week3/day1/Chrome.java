package Week3.day1;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.SendKeysAction;
import org.openqa.selenium.support.ui.Select;

public class Chrome {
	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver","./Driver/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.get("http://leaftaps.com/opentaps/");
		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Create Lead").click();
		WebElement cr=driver.findElementById("createLeadForm_dataSourceId");
		Select crobj= new Select(cr);
		crobj.selectByVisibleText("Conference");
		WebElement cr1=driver.findElementById("createLeadForm_marketingCampaignId");
		Select crobj2= new Select(cr1);
		crobj2.selectByValue("CATRQ_CARNDRIVER");
		List<WebElement> allop=crobj2.getOptions();
		for(WebElement eachoption:allop) {
			System.out.println(eachoption.getText());
		}
	}
}

